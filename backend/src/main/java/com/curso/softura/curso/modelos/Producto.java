package com.curso.softura.curso.modelos;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "m_producto")
public class Producto extends BitacoraModelo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long idProducto;

    @Column
    private String nombre;

    @Column
    private Integer precio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_producto")
    private TipoProducto tipoProducto;

    @OneToMany(mappedBy = "producto")
    private Set<ProductoAlmacen> productoAlmacenList;

    @OneToMany(mappedBy = "venta")
    private Set<ProductoVenta> productoVentaList;

    public Long getIdProducto() {
        return idProducto;
    }

    public Producto setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public Producto setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public TipoProducto getTipoProducto() {
        return tipoProducto;
    }

    public Producto setTipoProducto(TipoProducto tipoProducto) {
        this.tipoProducto = tipoProducto;
        return this;
    }

    public Set<ProductoAlmacen> getProductoAlmacenList() {
        return productoAlmacenList;
    }

    public void setProductoAlmacenList(Set<ProductoAlmacen> productoAlmacenList) {
        this.productoAlmacenList = productoAlmacenList;
    }

    public void setProductoAlmacenList(List<Almacen> listaAlmacen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Set<ProductoVenta> getProductoVentaList() {
        return productoVentaList;
    }

    public void setProductoVentaList(Set<ProductoVenta> productoVentaList) {
        this.productoVentaList = productoVentaList;
    }
}
