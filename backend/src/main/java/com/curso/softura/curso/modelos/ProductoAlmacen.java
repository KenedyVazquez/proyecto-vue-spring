/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.modelos;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Kenedy
 */
@Entity
@Table(name = "r_producto_almacen")
public class ProductoAlmacen extends BitacoraModelo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_producto_almacen")
    private Long idProductoAlmacen;

    @ManyToOne
    @JoinColumn(name = "id_producto")
    private Producto producto;
            
    @ManyToOne
    @JoinColumn(name = "id_almacen")
    private Almacen almacen;

    public Long getIdProductoAlmacen() {
        return idProductoAlmacen;
    }

    public void setIdProductoAlmacen(Long idProductoAlmacen) {
        this.idProductoAlmacen = idProductoAlmacen;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }
}
