package com.curso.softura.curso.modelos;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author Kenedy
 */
@Entity
@Table(name = "m_almacen")
public class Almacen extends BitacoraModelo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_almacen")
    private Integer idAlmacen;
    @Column(name = "nombre_almacen")
    private String nombreAlmacen;

    @OneToMany(mappedBy = "almacen")
    private Set<ProductoAlmacen> productoAlmacenList;

    public Integer getIdAlmacen() {
        return idAlmacen;
    }

    public void setIdAlmacen(Integer idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public String getNombreAlmacen() {
        return nombreAlmacen;
    }

    public void setNombreAlmacen(String nombreAlmacen) {
        this.nombreAlmacen = nombreAlmacen;
    }

    public Set<ProductoAlmacen> getProductoAlmacenList() {
        return productoAlmacenList;
    }

    public void setProductoAlmacenList(Set<ProductoAlmacen> productoAlmacenList) {
        this.productoAlmacenList = productoAlmacenList;
    }
}
