/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.productos.controlador;

import com.curso.softura.curso.general.RespuestaServicio;
import com.curso.softura.curso.productos.dto.TipoProductoDto;
import com.curso.softura.curso.productos.servicio.TipoProductoServicio;
import java.io.Serializable;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Kenedy
 */
@RestController
@RequestMapping("api/tipoproducto")
public class TipoProductoControlador implements Serializable {
    private static final long serialVersionUID = 8L;
     private transient Logger log = LoggerFactory.getLogger(TipoProductoControlador.class);
     
     @Autowired
     private transient  TipoProductoServicio tipoProductoServicio;
     @GetMapping("/obtener")
      public ResponseEntity<RespuestaServicio<List<TipoProductoDto>>> obtenerProductosActivos(){
          final RespuestaServicio<List<TipoProductoDto>> respuesta = new RespuestaServicio<>();
          try {
            List<TipoProductoDto> lista = tipoProductoServicio.obtenerTipoProductos();
            return respuesta.obtenerRespuesta(lista, "Datos obtenidos correctamente");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return respuesta.obtenerRespuestaError("Ocurrio un error al obtener los datos");
        }
      }
}
