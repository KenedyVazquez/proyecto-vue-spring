/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.productos.servicio;

import com.curso.softura.curso.modelos.TipoProducto;
import com.curso.softura.curso.productos.dto.TipoProductoDto;
import com.curso.softura.curso.respositorios.TipoProductoRepositorio;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kenedy
 */
@Service
@Transactional
public class TipoProductoServicio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Autowired
     private transient TipoProductoRepositorio tipoProductoRepositorio;
    
    public List<TipoProductoDto> obtenerTipoProductos() {
        List<TipoProducto> listaTipoProducto = tipoProductoRepositorio.findAll();
         List<TipoProductoDto> lista= new ArrayList<>();
           for (TipoProducto tipoProducto : listaTipoProducto) {
                   lista.add(new TipoProductoDto(tipoProducto.getIdTipoProducto(),tipoProducto.getTipoProducto()));
           }
           return lista;
    }
            
}
