/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.productos.servicio;

import com.curso.softura.curso.modelos.Almacen;
import com.curso.softura.curso.productos.dto.AlmacenDTO;
import com.curso.softura.curso.respositorios.AlmacenRepositorio;
import java.io.Serializable;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;
/**
 *
 * @author Kenedy
 */
@Service
@Transactional
public class AlmacenServicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Autowired
    private transient AlmacenRepositorio almacenRepositorio;
    
   public List<AlmacenDTO> obtenerAlmacenes(){
        List<Almacen> listaAlmacenes = almacenRepositorio.findAll();
  return listaAlmacenes.stream().map(almacen ->
                new AlmacenDTO( almacen.getIdAlmacen(), almacen.getNombreAlmacen(),
                        almacen.getRegistroActivo())
        ).collect(Collectors.toList());
   }
}
