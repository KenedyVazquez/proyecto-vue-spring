/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.respositorios;

import com.curso.softura.curso.modelos.Almacen;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kenedy
 */
@Repository
public interface AlmacenRepositorio extends JpaRepository<Almacen, Integer>  {
   
}
