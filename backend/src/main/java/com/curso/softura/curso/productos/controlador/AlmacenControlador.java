/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.productos.controlador;

import com.curso.softura.curso.productos.dto.AlmacenDTO;
import com.curso.softura.curso.productos.servicio.AlmacenServicio;
import java.io.Serializable;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.curso.softura.curso.general.RespuestaServicio;
import org.springframework.http.ResponseEntity;
/**
 *
 * @author Kenedy
 */
@RestController
@RequestMapping("api/almacen")
public class AlmacenControlador implements Serializable {
        private static final long serialVersionUID = 8366742694834006767L;

    private transient Logger log = LoggerFactory.getLogger(AlmacenControlador.class);
        @Autowired
    private transient AlmacenServicio almacenServicio;

    @GetMapping("/obtener")
    public ResponseEntity<RespuestaServicio<List<AlmacenDTO>>> obtenerAlmacenesActivos(){
        final RespuestaServicio<List<AlmacenDTO>> respuesta = new RespuestaServicio<>();

        try {
            List<AlmacenDTO> lista = almacenServicio.obtenerAlmacenes();
            return respuesta.obtenerRespuesta(lista, "Datos obtenidos correctamente");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return respuesta.obtenerRespuestaError("Ocurrio un error al obtener los datos");
        }
    }
}
