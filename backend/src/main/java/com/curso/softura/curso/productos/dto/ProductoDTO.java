package com.curso.softura.curso.productos.dto;

import com.curso.softura.curso.modelos.Almacen;
import com.curso.softura.curso.modelos.ProductoAlmacen;
import com.curso.softura.curso.modelos.TipoProducto;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class ProductoDTO implements Serializable {

    private static final long serialVersionUID = 5618541332556323433L;
    private Long idProducto;
    private String nombre;
    private Integer precio;
    private Integer cantidad;
    private Integer idTipoProducto;
    private Boolean activo;
    private TipoProducto tipoProducto;
    private List<AlmacenDTO> listaAlmacen;

    public ProductoDTO() {
        super();
    }

    public ProductoDTO(Long idProducto, String nombre, Integer precio, Integer idTipoProducto, Boolean activo, List<AlmacenDTO> listaAlmacen) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.precio = precio;
        this.idTipoProducto = idTipoProducto;
        this.activo = activo;
        this.listaAlmacen = listaAlmacen;
    }

    public ProductoDTO(Long idProducto, String nombre, Integer precio, Boolean activo, Integer idTipoProducto) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.precio = precio;
        this.activo = activo;
        this.idTipoProducto = idTipoProducto;
    }
    
    public ProductoDTO(Long idProducto, String nombre, Integer precio, Integer cantidad, Boolean activo, Integer idTipoProducto) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = precio;
        this.activo = activo;
        this.idTipoProducto = idTipoProducto;
    }
 
    public Long getIdProducto() {
        return idProducto;
    }

    public ProductoDTO setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public ProductoDTO setNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdTipoProducto() {
        return idTipoProducto;
    }

    public ProductoDTO setIdTipoProducto(Integer idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
        return this;
    }

    public Boolean getActivo() {
        return activo;
    }

    public ProductoDTO setActivo(Boolean activo) {
        this.activo = activo;
        return this;
    }

    public List<AlmacenDTO> getListaAlmacen() {
        return listaAlmacen;
    }

    public void setListaAlmacen(List<AlmacenDTO> listaAlmacen) {
        this.listaAlmacen = listaAlmacen;
    }

    public TipoProducto getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(TipoProducto tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

}
