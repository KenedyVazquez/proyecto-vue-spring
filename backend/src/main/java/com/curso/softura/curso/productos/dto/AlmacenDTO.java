/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.productos.dto;

import java.io.Serializable;

/**
 *
 * @author Kenedy
 */
public class AlmacenDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer idAlmacen;
    private String nombreAlmacen;
    private Boolean activo;
    public AlmacenDTO() {
        super();
    }

    public AlmacenDTO(Integer idAlmacen, String nombreAlmacen, Boolean activo) {
        this.idAlmacen = idAlmacen;
        this.nombreAlmacen = nombreAlmacen;
        this.activo = activo;
    }

    public Integer getIdAlmacen() {
        return idAlmacen;
    }

    public void setIdAlmacen(Integer idAlmacen) {
        this.idAlmacen = idAlmacen;
    }

    public String getNombreAlmacen() {
        return nombreAlmacen;
    }

    public void setNombreAlmacen(String nombreAlmacen) {
        this.nombreAlmacen = nombreAlmacen;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }


}
