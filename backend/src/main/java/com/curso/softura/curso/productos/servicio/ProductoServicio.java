package com.curso.softura.curso.productos.servicio;

import com.curso.softura.curso.helper.FechasHelper;
import com.curso.softura.curso.modelos.Almacen;
import com.curso.softura.curso.modelos.Producto;
import com.curso.softura.curso.modelos.ProductoAlmacen;
import com.curso.softura.curso.modelos.TipoProducto;
import com.curso.softura.curso.productos.dto.AlmacenDTO;
import com.curso.softura.curso.productos.dto.ProductoDTO;
import com.curso.softura.curso.respositorios.AlmacenRepositorio;
import com.curso.softura.curso.respositorios.ProductoAlmacenRepositorio;
import com.curso.softura.curso.respositorios.ProductoRepositorio;
import com.curso.softura.curso.respositorios.TipoProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductoServicio implements Serializable {

    private static final long serialVersionUID = 5587697424738022135L;

    @Autowired
    private transient ProductoRepositorio productoRepositorio;

    @Autowired
    private transient TipoProductoRepositorio tipoProductoRepositorio;

    @Autowired
    private transient ProductoAlmacenRepositorio productoAlmacenRepositorio;

    @Autowired
    private transient AlmacenRepositorio almacenRepositorio;

    public List<ProductoDTO> obtenerProductos() {
        List<Producto> lista = productoRepositorio.findAll();
        List<ProductoDTO> respuesta = new ArrayList<>();
        TipoProducto tipoProducto = new TipoProducto();
        for (Producto producto : lista) {
            List<ProductoAlmacen> listaAlmacen = productoAlmacenRepositorio.buscarAlmacenesPorId(producto.getIdProducto());
            List<AlmacenDTO> listaAlma = new ArrayList<>();
            for (ProductoAlmacen almacen : listaAlmacen) {
                if (almacen.getRegistroActivo() == true) {
                    listaAlma.add(new AlmacenDTO(almacen.getAlmacen().getIdAlmacen(), almacen.getAlmacen().getNombreAlmacen(), almacen.getRegistroActivo()));
                }
            }
            respuesta.add(new ProductoDTO(
                    producto.getIdProducto(),
                    producto.getNombre(),
                    producto.getPrecio(),
                    producto.getTipoProducto().getIdTipoProducto(),
                    producto.getRegistroActivo(),
                    listaAlma));
        }

        return respuesta;
    }

    public Boolean crearProducto(ProductoDTO productoDto) {
        Producto producto = new Producto();

        Optional<TipoProducto> tipoProducto = tipoProductoRepositorio.findById(productoDto.getIdTipoProducto());

        if (!tipoProducto.isPresent()) {
            return false;
        }

        if (productoDto.getIdProducto() != null) {
            producto = productoRepositorio.findById(productoDto.getIdProducto()).get();
        } else {
            producto.setFechaAlta(FechasHelper.obtenerFechaActual());
            producto.setUsuarioAlta((long) 1);
        }

        producto.setNombre(productoDto.getNombre());
        producto.setTipoProducto(tipoProducto.get());
        producto.setFechaModificacion(FechasHelper.obtenerFechaActual());
        producto.setUsuarioModificacion((long) 1);
        producto.setRegistroActivo(true);
        producto.setPrecio(productoDto.getPrecio());
        producto = productoRepositorio.save(producto);

        List<ProductoAlmacen> listaAlmacenes = productoAlmacenRepositorio.buscarAlmacenesPorId(producto.getIdProducto());
        if (listaAlmacenes.size() != 0) {
            for (ProductoAlmacen almacenproduct : listaAlmacenes) {
                almacenproduct.setRegistroActivo(false);
                productoAlmacenRepositorio.save(almacenproduct);
            }
        }

        for (AlmacenDTO almacen : productoDto.getListaAlmacen()) {
            boolean existe = false;
            for (ProductoAlmacen productoAlmacen : listaAlmacenes) {
                if (almacen.getIdAlmacen().equals(productoAlmacen.getAlmacen().getIdAlmacen())) {
                    productoAlmacen.setRegistroActivo(true);
                    productoAlmacenRepositorio.save(productoAlmacen);
                    existe = true;
                }

            }
            if (!existe) {
                ProductoAlmacen productoAlmacena = new ProductoAlmacen();
                productoAlmacena.setProducto(producto);
                Optional<Almacen> almace = almacenRepositorio.findById(almacen.getIdAlmacen());
                productoAlmacena.setAlmacen(almace.get());
                productoAlmacena.setUsuarioAlta((long) 1);
                productoAlmacena.setUsuarioModificacion((long) 1);
                productoAlmacena.setFechaAlta(FechasHelper.obtenerFechaActual());
                productoAlmacena.setFechaModificacion(FechasHelper.obtenerFechaActual());
                productoAlmacena.setRegistroActivo(true);
                productoAlmacenRepositorio.save(productoAlmacena);
            }

        }
        return true;

    }

    public Boolean deleteRegistro(Long id) {
        Optional<Producto> datos = productoRepositorio.findById(id);

        if (!datos.isPresent()) {
            return false;
        }

        datos.get().setRegistroActivo(!datos.get().getRegistroActivo());
        datos.get().setFechaModificacion(FechasHelper.obtenerFechaActual());
        datos.get().setUsuarioModificacion((long) 1);

        productoRepositorio.save(datos.get());
     
        return true;

    }
    
        public List<ProductoDTO> listarProducto(){
        List<Producto> lista = productoRepositorio.findAll();

        return lista.stream().map(producto ->
                new ProductoDTO(producto.getIdProducto(),producto.getNombre(),
                        producto.getPrecio(),producto.getRegistroActivo(), producto.getTipoProducto().getIdTipoProducto())
        ).collect(Collectors.toList());

    }


}
