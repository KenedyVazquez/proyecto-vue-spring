/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.curso.softura.curso.respositorios;

import com.curso.softura.curso.modelos.Almacen;
import com.curso.softura.curso.modelos.ProductoAlmacen;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kenedy
 */
@Repository
public interface ProductoAlmacenRepositorio extends JpaRepository<ProductoAlmacen, Long> {

    /*SELECT * FROM 
r_producto_almacen rpa
INNER JOIN m_almacen ma ON rpa.id_almacen = ma.id_almacen
WHERE rpa.id_producto = 4 */

    @Query("select pa from ProductoAlmacen pa"
            + " inner join Almacen al on pa.almacen.idAlmacen = al.idAlmacen "
            + "where pa.producto.idProducto = ?1")
    List<ProductoAlmacen> buscarAlmacenesPorId(Long idProducto);
}
