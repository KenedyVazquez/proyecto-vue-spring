PGDMP     *    ,                 y            curso    10.15    10.15                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16451    curso    DATABASE     �   CREATE DATABASE curso WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Mexico.1252' LC_CTYPE = 'Spanish_Mexico.1252';
    DROP DATABASE curso;
             postgres    false                        2615    16452    curso    SCHEMA        CREATE SCHEMA curso;
    DROP SCHEMA curso;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16453    c_tipo_producto    TABLE     m  CREATE TABLE curso.c_tipo_producto (
    id_tipo_producto integer NOT NULL,
    tipo_producto character varying NOT NULL,
    usuario_alta bigint NOT NULL,
    usuario_modificacion bigint NOT NULL,
    fecha_alta timestamp without time zone NOT NULL,
    fecha_modificacion timestamp without time zone NOT NULL,
    registro_activo boolean DEFAULT true NOT NULL
);
 "   DROP TABLE curso.c_tipo_producto;
       curso         postgres    false    5            �            1259    16460    sec_almacen    SEQUENCE     s   CREATE SEQUENCE curso.sec_almacen
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE curso.sec_almacen;
       curso       postgres    false    5            �            1259    16462 	   m_almacen    TABLE     �  CREATE TABLE curso.m_almacen (
    id_almacen integer DEFAULT nextval('curso.sec_almacen'::regclass) NOT NULL,
    nombre_almacen character varying NOT NULL,
    usuario_alta bigint NOT NULL,
    usuario_modificacion bigint NOT NULL,
    fecha_alta timestamp without time zone NOT NULL,
    fecha_modificacion timestamp without time zone NOT NULL,
    registro_activo boolean DEFAULT true NOT NULL
);
    DROP TABLE curso.m_almacen;
       curso         postgres    false    198    5            �            1259    16470    sec_producto    SEQUENCE     t   CREATE SEQUENCE curso.sec_producto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE curso.sec_producto;
       curso       postgres    false    5            �            1259    16472 
   m_producto    TABLE     �  CREATE TABLE curso.m_producto (
    id_producto bigint DEFAULT nextval('curso.sec_producto'::regclass) NOT NULL,
    nombre character varying NOT NULL,
    usuario_alta bigint NOT NULL,
    usuario_modificacion bigint NOT NULL,
    fecha_alta timestamp without time zone NOT NULL,
    fecha_modificacion timestamp without time zone NOT NULL,
    registro_activo boolean DEFAULT true NOT NULL,
    id_tipo_producto integer NOT NULL
);
    DROP TABLE curso.m_producto;
       curso         postgres    false    200    5            �            1259    16480    sec_producto_almacen    SEQUENCE     �   CREATE SEQUENCE curso.sec_producto_almacen
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 454545115451
    CACHE 1;
 *   DROP SEQUENCE curso.sec_producto_almacen;
       curso       postgres    false    5            �            1259    16482    r_producto_almacen    TABLE     �  CREATE TABLE curso.r_producto_almacen (
    id_producto_almacen bigint DEFAULT nextval('curso.sec_producto_almacen'::regclass) NOT NULL,
    id_producto bigint NOT NULL,
    id_almacen integer NOT NULL,
    usuario_alta bigint NOT NULL,
    usuario_modificacion bigint NOT NULL,
    fecha_alta timestamp without time zone NOT NULL,
    fecha_modificacion timestamp without time zone NOT NULL,
    registro_activo boolean DEFAULT true NOT NULL
);
 %   DROP TABLE curso.r_producto_almacen;
       curso         postgres    false    202    5                      0    16453    c_tipo_producto 
   TABLE DATA               �   COPY curso.c_tipo_producto (id_tipo_producto, tipo_producto, usuario_alta, usuario_modificacion, fecha_alta, fecha_modificacion, registro_activo) FROM stdin;
    curso       postgres    false    197   n$                 0    16462 	   m_almacen 
   TABLE DATA               �   COPY curso.m_almacen (id_almacen, nombre_almacen, usuario_alta, usuario_modificacion, fecha_alta, fecha_modificacion, registro_activo) FROM stdin;
    curso       postgres    false    199   �$                 0    16472 
   m_producto 
   TABLE DATA               �   COPY curso.m_producto (id_producto, nombre, usuario_alta, usuario_modificacion, fecha_alta, fecha_modificacion, registro_activo, id_tipo_producto) FROM stdin;
    curso       postgres    false    201   >%                 0    16482    r_producto_almacen 
   TABLE DATA               �   COPY curso.r_producto_almacen (id_producto_almacen, id_producto, id_almacen, usuario_alta, usuario_modificacion, fecha_alta, fecha_modificacion, registro_activo) FROM stdin;
    curso       postgres    false    203   H&                  0    0    sec_almacen    SEQUENCE SET     9   SELECT pg_catalog.setval('curso.sec_almacen', 1, false);
            curso       postgres    false    198                       0    0    sec_producto    SEQUENCE SET     9   SELECT pg_catalog.setval('curso.sec_producto', 9, true);
            curso       postgres    false    200                       0    0    sec_producto_almacen    SEQUENCE SET     B   SELECT pg_catalog.setval('curso.sec_producto_almacen', 1, false);
            curso       postgres    false    202            �
           2606    16488 $   c_tipo_producto c_tipo_producto_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY curso.c_tipo_producto
    ADD CONSTRAINT c_tipo_producto_pkey PRIMARY KEY (id_tipo_producto);
 M   ALTER TABLE ONLY curso.c_tipo_producto DROP CONSTRAINT c_tipo_producto_pkey;
       curso         postgres    false    197            �
           2606    16490    m_almacen m_almacen_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY curso.m_almacen
    ADD CONSTRAINT m_almacen_pkey PRIMARY KEY (id_almacen);
 A   ALTER TABLE ONLY curso.m_almacen DROP CONSTRAINT m_almacen_pkey;
       curso         postgres    false    199            �
           2606    16492    m_producto m_producto_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY curso.m_producto
    ADD CONSTRAINT m_producto_pkey PRIMARY KEY (id_producto);
 C   ALTER TABLE ONLY curso.m_producto DROP CONSTRAINT m_producto_pkey;
       curso         postgres    false    201            �
           2606    16494 *   r_producto_almacen r_producto_almacen_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY curso.r_producto_almacen
    ADD CONSTRAINT r_producto_almacen_pkey PRIMARY KEY (id_producto_almacen);
 S   ALTER TABLE ONLY curso.r_producto_almacen DROP CONSTRAINT r_producto_almacen_pkey;
       curso         postgres    false    203            �
           2606    16495    m_producto m_producto_fk    FK CONSTRAINT     �   ALTER TABLE ONLY curso.m_producto
    ADD CONSTRAINT m_producto_fk FOREIGN KEY (id_tipo_producto) REFERENCES curso.c_tipo_producto(id_tipo_producto);
 A   ALTER TABLE ONLY curso.m_producto DROP CONSTRAINT m_producto_fk;
       curso       postgres    false    2698    201    197            �
           2606    16500 1   r_producto_almacen r_producto_almacen__almacen_fk    FK CONSTRAINT     �   ALTER TABLE ONLY curso.r_producto_almacen
    ADD CONSTRAINT r_producto_almacen__almacen_fk FOREIGN KEY (id_almacen) REFERENCES curso.m_almacen(id_almacen);
 Z   ALTER TABLE ONLY curso.r_producto_almacen DROP CONSTRAINT r_producto_almacen__almacen_fk;
       curso       postgres    false    203    199    2700            �
           2606    16505 (   r_producto_almacen r_producto_almacen_fk    FK CONSTRAINT     �   ALTER TABLE ONLY curso.r_producto_almacen
    ADD CONSTRAINT r_producto_almacen_fk FOREIGN KEY (id_producto) REFERENCES curso.m_producto(id_producto);
 Q   ALTER TABLE ONLY curso.r_producto_almacen DROP CONSTRAINT r_producto_almacen_fk;
       curso       postgres    false    201    2702    203               Q   x�3�t��-��4B##]C#]Cc#c+s+S#�b%\F����E�yə�0�����F�
�V��V&�X�́Zc���� ���         _   x�m�1
�0F�99E/�4m��:	R;x|;����?���&i!B�(� �a5��*��]����5�O-JcJ����K'S������5y�����#u         �   x�}�MN�@���Y#���x<��p 6�	
-���3D��U��G��������%j��q Hz�@��Hמ$CJ�J�^u%�������̡䠖i�p��T,���Xz[���h!0��9��ְ D^�p�����x�1�-:`�J�aDڙ �RʚE~�����B�6��Vs⮦�qs��L�=��J�W+ʡ
ƌ�fɷ�d�,ൈ��)�}�֯�EoB�P\���Jnw)�{^�G���Kp�         �   x�m�Q!E�o\�l`x�
k�2���N��ĿkN�(�H>9YN�C��y�_��uz�����ŵ��ހ0���ݠ��Ѥ�0��6����0J=��5ikN�����f��0#L:g�"if~�lf��{3gc4sc�=iF�f��a{��Db�l#�'�z>J)oU�eM     